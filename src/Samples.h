#ifndef SAMPLES_H
#define SAMPLES_H

#include "Project.h"

static default_random_engine rng(random_device{}());
static const int GRID_SIZE = 36;

/**
 * Generates multi-jittered sampling coords in range (-0.5,5).
 * @returns list of sample coordinates
 */
vector<pair<double,double>> getJitteredSamples() {
	const int n = GRID_SIZE;
	int rows[n*n], cols[n*n];
	double bigs = 1.0/n;
	double smalls = 1.0/(n*n);
	uniform_real_distribution<double> dDist(0,smalls);
	vector<pair<double,double>> result;
	
	for (int i=0; i<n; ++i) {
		int* rowi = rows+i*n;
		int* coli = cols+i*n;
		for (int j=0; j<n; ++j) {
			rowi[j] = j;
			coli[j] = j;
		}
		shuffle(rowi, rowi+n, rng);
		shuffle(coli, coli+n, rng);
	}
	for (int i=0; i<n; ++i) {
		for (int j=0; j<n; ++j) {
			double x = i*bigs + rows[i*n+j]*smalls + dDist(rng);
			double y = j*bigs + cols[j*n+i]*smalls + dDist(rng);
			result.push_back({x-0.5,y-0.5});
		}
	}
	return result;
}

#endif
