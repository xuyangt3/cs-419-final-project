#ifndef HITTABLE_H
#define HITTABLE_H

#include "Project.h"
#include "Ray.h"
#include "AABB.h"

static const double EPSILON = 0.0001;

class Material;
extern const shared_ptr<Material> MAT_DEFAULT;

class HitRecord {
public:
	double t;
	Vec pos;
	Vec normal;
	shared_ptr<Material> matPtr;
};

class Hittable {
public:
	shared_ptr<Material> matPtr = MAT_DEFAULT;

	/**
	 * Calculates if the Ray hits this object.
	 * Writes the nearest hit to record.
	 * @param ray - ray to calculate
	 * @param minT - minimum t for the hit to be considered
	 * @param maxT - maximum t for the hit to be considered
	 * @param record - HitRecord to write to
	 * @returns true if a hit occurs
	 */
	virtual bool hit(const Ray& ray, double minT, double maxT, HitRecord& record) const = 0;
	virtual bool getAABB(AABB& result) const = 0;
	/**
	 * Generates extra (biased) sample rays.
	 * @src source of the requested sample ray
	 * @returns Ray sample
	 */
	virtual Ray sample(const Vec& src) const = 0;
	/**
	 * Calculate the pdf of a ray to be obtained from sample()
	 * @ray the ray
	 * @returns pdf of occurence
	 */
	virtual double sample_pdf(const Ray& ray) const = 0;
};

class Plane : public Hittable {
public:
	Vec anchor;
	Vec normal;

	/**
	 * Constructor.
	 * @param anchorPoint - a point on the plane
	 * @param upperNormal - plane normal pointing up
	 */
	Plane(const Vec& anchorPoint, const Vec& upperNormal) {
		anchor = anchorPoint;
		normal = upperNormal;
	}

	//See Hittable.
	bool hit(const Ray& ray, double minT, double maxT, HitRecord& record) const {
		
		double proj = normal.dot(ray.dir);
		if (-EPSILON < proj && proj < EPSILON) return false;
		double t = normal.dot(anchor - ray.src) / proj;
		if (t < minT  || maxT < t) return false;
		record.t = t;
		record.pos = ray.at(t);
		record.normal = normal;
		record.matPtr = matPtr;
		return true;
	}
	
	bool getAABB(AABB& result) const {
		return false;
	}
	//todo
	Ray sample(const Vec& src) const{};
	double sample_pdf(const Ray& ray) const {return 0;};
};

class Parallelogram : public Hittable {
public:
	Vec anchor;
	double area;
	Matrix3d basis;
	Matrix3d invBasis;
	Parallelogram(const Vec& anchorPoint, const Vec& edge1, const Vec& edge2) {
		anchor = anchorPoint;
		area = edge1.cross(edge2).norm();
		basis.col(0) = edge1;
		basis.col(1) = edge2;
		basis.col(2) = edge1.cross(edge2).normalized();
		invBasis = basis.inverse();
	}
	bool hit(const Ray& ray, double minT, double maxT, HitRecord& record) const {
		HitRecord temp;
		Plane p = {anchor, basis.col(2)};
		if (!p.hit(ray, minT, maxT, temp)) return false;
		Vec coord = invBasis*(temp.pos-anchor);
		auto uv = coord.head(2).array();
		if ((uv<0.0).any() || (uv>1.0).any()) return false;
		record = temp;
		record.matPtr = matPtr;
		if (basis.col(2).dot(ray.dir)>0) {
			record.normal = -basis.col(2);
		}
		return true;
	}
	bool getAABB(AABB& result) const {
		result = {anchor,
			anchor+basis.col(0),
			anchor+basis.col(1),
			anchor+basis.col(0)+basis.col(1)};
		return true;
	}
	Ray sample(const Vec& src) const {
		Vector2d t = (Vector2d::Random().array()+1)*0.5;
		return {src, (anchor + basis.leftCols(2)*t - src).normalized()};
	}
	virtual double sample_pdf(const Ray& ray) const {
	    HitRecord rec;
	    if (!this->hit(ray, EPSILON, INFINITY, rec)) return 0;
		return (rec.pos-ray.src).squaredNorm()/fabs(area*rec.normal.dot(ray.dir.normalized()));
	}
};

class Sphere : public Hittable {
public:
	Vec center;
	double radius;
	
	/**
	 * Constructor.
	 * @param sphereCenter - center of the sphere
	 * @param sphereRadius - radius
	 */
	Sphere(const Vec& sphereCenter, const double sphereRadius) {
		center = sphereCenter;
		radius = sphereRadius;
	}
	
	//See Hittable.
	bool hit(const Ray& ray, double minT, double maxT, HitRecord& record) const {
        Vec toSrc = ray.src - center;
        double a = ray.dir.dot(ray.dir);
        double b = 2 * toSrc.dot(ray.dir);
        double c = toSrc.dot(toSrc) - radius*radius;
        
        Vec nDir = ray.dir.normalized();
		Vec l = toSrc - toSrc.dot(nDir)*nDir;
        double det = 4 * a * (radius*radius - l.dot(l));
        if (det <= 0) return false;
        
        double q = b>0? sqrt(det) : -sqrt(det);
        q = -0.5 * (b+q);
        double t = maxT;
        double t1, t2;
        if ((t1 = c/q) >= minT && t1 <= t) t = t1;
        if ((t2 = q/a) >= minT && t2 <= t) t = t2;
        if (t == maxT) return false;
        
		record.t = t;
		record.pos = ray.at(t);
		record.normal = (record.pos - center)*(1/radius);
		record.matPtr = matPtr;
        
		return true;
	}
	
	bool getAABB(AABB& result) const {
		result = {center.array()-radius, center.array()+radius};
		return true;
	}
	Ray sample(const Vec& src) const {
		Vec toCenter = center - src;
		//randomn (0,1)
		Vector2d t = (Vector2d::Random().array()+1)*0.5;
		double phi = 2*M_PI*t[0];
		
		double z = 1 + t[1]*(sqrt(1-radius*radius/toCenter.squaredNorm()) - 1);
		double x = cos(phi)*sqrt(1-z*z);
		double y = sin(phi)*sqrt(1-z*z);
		
		Vec dir = Quaterniond::FromTwoVectors(Vec(0,0,1),toCenter)*Vec(x,y,z);
		return Ray(src, dir);
	};
	double sample_pdf(const Ray& ray) const {
	    HitRecord ignored;
	    if (!this->hit(ray, EPSILON, INFINITY, ignored)) return 0;
	    
	    double cos_theta_max = sqrt(1 - radius*radius/(center-ray.src).squaredNorm());
	    double solid_angle = 2*M_PI*(1-cos_theta_max);
	
	    return  1 / solid_angle;
	};
};

class Triangle : public Hittable {
public:
	Vec a;
	Vec b;
	Vec c;
	bool interpolate = false;
	Vec nA;
	Vec nB;
	Vec nC;
	
	/**
	 * Constructor. Both sides of the trianble are visible.
	 * @param vertA - vertex 1
	 * @param vertB - vertex 2
	 * @param vertC - vertex 3
	 */
	Triangle(const Vec& vertA, const Vec& vertB, const Vec& vertC) {
		a = vertA;
		b = vertB;
		c = vertC;
	}
	
	//See Hittable.
	bool hit(const Ray& ray, double minT, double maxT, HitRecord& record) const {
        Vec e1 = b - a;
        Vec e2 = c - a;
        Vec s = a - ray.src;
        Vec normal = e1.cross(e2);
        
		double proj = normal.dot(ray.dir);
		if (-EPSILON < proj && proj < EPSILON) {
			return false;
		}
		double t = normal.dot(s) / proj;
		if (t < minT  || maxT < t) return false;
		
		Vec m = s.cross(ray.dir);
		double u,v;
		
        if ((u = m.dot(e2)/proj) < 0 || 1 < u) return false;
        if ((v = -m.dot(e1)/proj) < 0 || 1 < v) return false;
		if (u+v > 1) return false;
        
		record.t = t;
		record.pos = ray.at(t);
		record.matPtr = matPtr;

		if (interpolate) {
			record.normal = (1-u-v)*nA + u*nB + v*nC;
		} else {
			record.normal = normal.normalized();
		}
		if (proj > 0) {
			record.normal = -record.normal;
		}
		
		return true;
	}
	
	bool getAABB(AABB& result) const {
		result = {a,b,c};
		return true;
	}
	
	void setNormals(const Vec& normA, const Vec& normB, const Vec& normC) {
		interpolate = true;
		nA = normA;
		nB = normB;
		nC = normC;
	}
	//todo
	Ray sample(const Vec& src) const {};
	double sample_pdf(const Ray& ray) const {return 0;};
};

#endif
