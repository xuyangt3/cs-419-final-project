#ifndef BVH_NODE_H
#define BVH_NODE_H

#include "Project.h"
#include "Hittable.h"

class BVHNode : public Hittable {
	static const int LEAF_SIZE = 3;

public:
	bool leaf;
	shared_ptr<BVHNode> left;
	shared_ptr<BVHNode> right;
	vector<shared_ptr<Hittable>> primitives;
	
	/*
	Warning: list should never contain unbounded primitives!
	*/
	BVHNode(vector<shared_ptr<Hittable>> &primitiveList,
		size_t begin, size_t end) {
		
		AABB centers;

		for (size_t i=begin; i<end; ++i) {
			AABB tempBox;
			primitiveList[i]->getAABB(tempBox);
			box.join(tempBox);
			centers.addPoint(tempBox.getCenter());
		}
		leaf = (end-begin) <= LEAF_SIZE;
		if (leaf) {
			setPrimitives(primitiveList, begin, end);
			return;
		}
		Vec spread = centers.max - centers.min;
		int axis = 0;
		if (spread[axis] < spread[1]) axis = 1;
		if (spread[axis] < spread[2]) axis = 2;
		double pivot = centers.getCenter()[axis];
		auto midIter = partition(
			primitiveList.begin()+begin,
			primitiveList.begin()+end,
			[axis, pivot](const shared_ptr<Hittable>& p){
				AABB tempBox;
				p->getAABB(tempBox);
				return tempBox.getCenter()[axis] < pivot;
			}
		);
		size_t mid = midIter-primitiveList.begin();
		if (mid==begin || mid==end) {
			leaf = true;
			setPrimitives(primitiveList, begin, end);
			return;
		}
		left = make_shared<BVHNode>(primitiveList, begin, mid);
		right = make_shared<BVHNode>(primitiveList, mid, end);
	}
	
	bool getAABB(AABB& result) const {
		result = box;
		return true;
	}
	
	bool hit(const Ray& ray, double minT, double maxT, HitRecord& record) const {
		if (!box.hit(ray, minT, maxT)) return false;
		
		bool result = false;
		if (leaf) {
	        for (auto h : primitives) {
	        	result |= h -> hit(ray, minT, (result?record.t:maxT), record);
	        }
        	return result;
		} else {
        	result |= left -> hit(ray, minT, (result?record.t:maxT), record);
        	result |= right -> hit(ray, minT, (result?record.t:maxT), record);
        	return result;
		}
	}
	//todo
	Ray sample(const Vec& src) const{};
	double sample_pdf(const Ray& ray) const {return 0;};

private:
	AABB box;
	
	void setPrimitives(vector<shared_ptr<Hittable>> &primitiveList,
		size_t begin, size_t end) {
			
		auto itrBegin = primitiveList.begin() + begin;
		auto itrEnd = primitiveList.begin() + end;
		primitives = vector<shared_ptr<Hittable>>(
			itrBegin, itrEnd);
	}
};

#endif
