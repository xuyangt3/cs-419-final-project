#ifndef HITTABLE_LIST_H
#define HITTABLE_LIST_H

#include "Project.h"
#include "Hittable.h"

class HittableList : public Hittable{
public:
	vector<shared_ptr<Hittable>> list;

	/**
	 * Checks if the ray hits any Hittable contained in the list.
	 * Writes the nearest hit to record.
	 * @param ray - ray to calculate
	 * @param minT - minimum t for the hit to be considered
	 * @param maxT - maximum t for the hit to be considered
	 * @param record - HitRecord to write to
	 * @returns true if any hit occurs
	 */
    bool hit(const Ray& ray, double minT, double maxT, HitRecord& record) const {
        bool result = false;

        for (const auto& h : list) {
        	result |= h -> hit(ray, minT, (result?record.t:maxT), record);
        }
        return result;
    }
    
	bool getAABB(AABB& result) const {
		return false;
	}
	//todo
	Ray sample(const Vec& src) const{};
	double sample_pdf(const Ray& ray) const {return 0;};
};

#endif
