#include "Project.h"
#include "View.h"
#include "HittableList.h"
#include "Material.h"
#include "Samples.h"
#include "PatchReader.h"
#include "BVHNode.h"

using Eigen::Translation3d;
using Eigen::Scaling;

class Scene {
	public:
		shared_ptr<View> camera;
		shared_ptr<Hittable> world;
		shared_ptr<Hittable> lights;
		RGB background;
};

/**
 * Monte Carlo, recursively evaluates color of ray
 * @param in - incoming ray
 * @param depth - recursion depth
 */
RGB rayColor(const Ray& in, int depth, const Scene& scene) {
	if (depth <= 0) return {0,0,0};
	const Hittable& world = *scene.world;

	HitRecord r;
	if (!world.hit(in,EPSILON,INFINITY,r)) return scene.background;
	Material& mat = *(r.matPtr);
	RGB emitted = mat.emitted();
	RGB attenuation;
	Ray scattered;
	if (!mat.scatter(in, r, *scene.lights, attenuation, scattered)) {
		return emitted;
	}
	return emitted + attenuation.cwiseProduct(rayColor(scattered, depth-1, scene));
}

/**
 * Performs the major render task. Sends the ppm output to stdout.
 */
void render(const Scene& scene) {
	auto start = std::chrono::high_resolution_clock::now();
	const View& camera = *scene.camera;
	cout << "P3\n" << camera.w << ' ' << camera.h << "\n255\n";

	for (int i=camera.h-1; i>=0; --i) {
		cerr << "\rScanlines remaining: " << i << " " << std::flush;
		for (int j=0; j<camera.w; ++j) {
			RGB clr(0,0,0);
			auto samples = getJitteredSamples();
			for (const auto& p : samples) {
				Ray in = camera.getRay(i+p.first,j+p.second);
				clr += clamp(rayColor(in, 50, scene));
			}
			clr = 255.99 * (clr*(1.0/samples.size())).cwiseSqrt();
			cout << (int)clr[0] << ' ' << (int)clr[1] << ' ' << (int)clr[2] << ' ';
		}
		cout << endl;
	}
	auto stop = std::chrono::high_resolution_clock::now();
	double duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start).count();
	cout << "# render time: " << duration/1000 << "s" << endl;
	cerr << "\nDone.\n";
	cerr << duration/1000 << " s\n";
}

Scene complexScene() {
    
    
	shared_ptr<HittableList> world = make_shared<HittableList>();
	Scene scene;
	scene.camera = make_shared<PerspectiveView>();
	scene.world = world;
	scene.background = {0.2,0.2,0.2};

	auto ground = make_shared<Plane>(Vec(0,-600,0), Vec(0,1,0));
	ground->matPtr = make_shared<CheckeredLambertian>(Vec(0.1,0.1,0.8), Vec(0.0, 0.0, 0.0), 100.0);
	world->list.push_back(ground);
	
	auto mirror = make_shared<Mirror>(Vec(1,1,1));
	auto tri0a = make_shared<Triangle>(
		Vec(0,-600,-2300),
		Vec(0,-600,-12000),
		Vec(-900,600,-12000));
	tri0a->matPtr = mirror;
	world->list.push_back(tri0a);
	
	auto tri0b = make_shared<Triangle>(
		Vec(-900,600,-2300),
		Vec(0,-600,-2300),
		Vec(-900,600,-12000));
	tri0b->matPtr = mirror;
	world->list.push_back(tri0b);
	
	auto sph1 = make_shared<Sphere>(Vec(600,0,-6000), 600);
    sph1->matPtr = make_shared<CheckeredLambertian>(Vec(0.1,0.8,0.1), Vec(0.0, 0.0, 0.0), 100.0);

	world->list.push_back(sph1);
	
	auto sph2 = make_shared<Sphere>(Vec(900,300,-4000), 300);
	sph2->matPtr = make_shared<Glass>(2);
	world->list.push_back(sph2);
	
	auto sph3 = make_shared<Sphere>(Vec(0,900,-3000), 400);
	sph3->matPtr = make_shared<Glass>(1.33);
	world->list.push_back(sph3);
	
	auto sph4 = make_shared<Sphere>(Vec(0,0,-9000), 600);
	sph4->matPtr = make_shared<Mirror>(Vec(1,1,1));
	world->list.push_back(sph4);
	
	Affine3d op = Translation3d(Vec(400,-610,-4700))*Scaling(150.)*
		AngleAxisd(-3.14*1/4,Vec(0,1,0));;
	world->list.push_back(make_shared<BVHNode>(readPatchFile("teapot.pts",op,MAT_DEFAULT,10,10)));
    
	scene.lights =  make_shared<Parallelogram>(Vec(-600,-300,-2000),Vec(0,1200,0),Vec(0,0,-2400));
	scene.lights->matPtr = make_shared<Emitter>(Vec(7,7,7));
	world->list.push_back(scene.lights);

	return scene;
}


/**
 * Entry point. Takes 1 argument as switches.
 */
int main (int argc, char *argv[]) {
	Scene scene = complexScene();
	render(scene);
}

