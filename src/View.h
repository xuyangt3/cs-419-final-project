#ifndef VIEW_H
#define VIEW_H

#include "Project.h"
#include "Ray.h"

static const double PX_CENTER = 0.5;

class View {
private:
	Vec right;
	double halfW;
	double halfH;
	
public:
	Vec ctr = {0,0,0};
	Vec dir = {0,0,-1};
	Vec up = {0,1,0};
	double pxSize = 1;
	int w = 400;
	int h = 400;
	
	/**
	 * Default constructor.
	 */
	View() {update();}
	
	/**
	 * Constructor.
	 * @param center - center of the view plane
	 * @param direction - direction of the eye
	 * @param viewUp - up vector
	 * @param pixelSize - size of each (square) pixel
	 * @param width - width of the view plane
	 * @param height - height of the view plane
	 */
	View(const Vec& center, const Vec& direction, const Vec& viewUp,
		double pixelSize, int width, int height) {
		
		ctr = center;
		dir = direction;
		up = viewUp;
		pxSize = pixelSize;
		w = width;
		h = height;
		update();
	}
	
	/**
	 * Updates direction vector, right vector, up vector.
	 * Updates halfW and halfH.
	 */
	void update() {
		dir.normalize();
		right = dir.cross(up).normalized();
		up = right.cross(dir);
		
		halfW = ((double) w)/2;
		halfH = ((double) h)/2;
	}
	
	/**
	 * Gets the world coordinate of screen position x,y.
	 * @param x - horizontal coordinate
	 * @param y - vertical coordinate
	 * @returns the coordinate
	 */
	Vec getCoord(double x, double y) const {
		return ctr + right*(y - halfW + PX_CENTER)*pxSize + up*(x - halfH + PX_CENTER)*pxSize;
	}
	
	/**
	 * Gets the ray casting from screen position x,y.
	 * @param x - horizontal coordinate
	 * @param y - vertical coordinate
	 * @returns the ray
	 */
	virtual Ray getRay(double x, double y) const = 0;
};

class OrthographicView : public View {
public:
	
	//Constructor. Same as View.
	OrthographicView() {
		pxSize = 5;
	}
	
	//Constructor. Same as View.
	OrthographicView(
		const Vec& center, const Vec& direction, const Vec& viewUp,
		double pixelSize, int width, int height) :
		View(center, direction, viewUp, pixelSize, width, height){}
	
	//Same as View.
	Ray getRay(double x, double y) const {
		return Ray(getCoord(x, y), dir);
	}
};

class PerspectiveView : public View {
public:
	double dist = 600;
	
	//Constructor. Same as View.
	PerspectiveView() {}
	
	/**
	 * Constructor. Adds 1 extra param.
	 * @param eyeDistance - distance of the plane to the eye
	 * @param center - center of the view plane
	 * @param direction - direction of the eye
	 * @param viewUp - up vector
	 * @param pixelSize - size of each (square) pixel
	 * @param width - width of the view plane
	 * @param height - height of the view plane
	 */
	PerspectiveView(
		double eyeDistance,
		const Vec& center, const Vec& direction, const Vec& viewUp,
		double pixelSize, int width, int height) :
		View(center, direction, viewUp, pixelSize, width, height){
        
		dist = eyeDistance;
	}
	
	//Same as View.
	Ray getRay(double x, double y) const {
        
		Vec eye = ctr - dir*dist;
		Vec coord = getCoord(x, y);
		return Ray(coord, coord - eye);
	}
};

#endif
