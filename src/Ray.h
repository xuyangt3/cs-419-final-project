#ifndef RAY_H
#define RAY_H

#include "Project.h"

class Ray {
public:
	Vec src;
	Vec dir;
	
	Ray () {}
	
	/**
	 * Constructor.
	 * @param source - where the ray starts
	 * @param direction - where the ray points to
	 */
	Ray (const Vec& source, const Vec& direction) {
		src = source;
		dir = direction;
	}
	
	/**
	 * Gets a point on the Ray's parametric curve.
	 * @param t - parameter of the Ray's curve
	 */
	inline Vec at(double t) const {
		return src+dir*t;
	}
	
	//for printing
	friend ostream& operator<< (ostream& out, const Ray& r) {
		return out << "(" << r.src.transpose() << ", " << r.dir.transpose() << ")\n";
	}
};

#endif
