#ifndef PATCH_READER_H
#define PATCH_READER_H

#include "Project.h"
#include "BVHNode.h"
#include "Bezier.h"

BVHNode readPatchFile(const char* patchFile,
                      const Affine3d& transform = Affine3d::Identity(),
                      const shared_ptr<Material> matPtr=MAT_DEFAULT,
					  int nx=3, int ny=3) {
	ifstream indata;
	indata.open(patchFile);
	if(!indata) {
		cerr << "Error opening patch file: " << patchFile << endl;
		exit(1);
	}
	vector<shared_ptr<Hittable>> faces;
	Matrix<double,3,16> ctrlPts;
	int i;
	double value;
	while (indata >> value) {
		ctrlPts.data()[i] = value;
		++i;
		if (i==3*16) {
			ctrlPts = transform*ctrlPts;
			genMesh(nx,ny,ctrlPts,faces,matPtr);
			i=0;
		}
	}
	if (i) {
		cerr << "Warning leftover data: " << i << endl;		
	}
	indata.close();
	return BVHNode(faces,0,faces.size());
}

#endif
