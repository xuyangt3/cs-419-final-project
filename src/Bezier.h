#ifndef BEZIER_H
#define BEZIER_H

#include "Project.h"
#include "Hittable.h"
#include "Material.h"

/**
 * The alternative and more stable way is to subdivide
 * a patch into 2*2 sub patches, but it seems uncecessary
 * for now.
 */
//Matrix4d subdivideWeights = (Matrix4d() <<
//	   1.,   0.,   0.,   0.,
//	  0.5,  0.5,   0.,   0.,
//	 0.25,  0.5, 0.25,   0.,
//    0.125,0.375,0.375,0.125).finished();

const Matrix<double,4,3> dirWeights = (Matrix<double,4,3>() <<
	-3,  6, -3,
	 9,-12,  3,
	-9,  6,  0,
	 3,  0,  0).finished();

const Matrix4d basisWeights = (Matrix4d() <<
	-1, 3,-3, 1,
	 3,-6, 3, 0,
	-3, 3, 0, 0,
	 1, 0, 0, 0).finished();

Vector4d basisCoeff(double t) {
	return basisWeights*Vector4d(t*t*t, t*t, t, 1);
}

Vector4d basisDir(double t) {
	return dirWeights*Vec(t*t, t, 1);
}

void genMesh(int nx, int ny, Matrix<double,3,16>& ctrlPts,
	vector<shared_ptr<Hittable>>& faces,
	const shared_ptr<Material> matPtr=MAT_DEFAULT) {
	double dx = 1./nx;
	double dy = 1./ny;
	// Store coordinates in 2k cols, normals in 2k+1 cols.
	// Store even ix in 4k cols, odd ix in 4k+2 cols.
	// Then we have good locality.
	MatrixXd prevs = MatrixXd(3, 4*(ny+1));
	for (int ix=0; ix<=nx; ++ix) {
		Vector4d xCoeff = basisCoeff(dx*ix);
		Vector4d xDir = basisDir(dx*ix);
		for (int iy=0; iy<=ny; ++iy) {
			Vector4d yCoeff = basisCoeff(dy*iy);
			MatrixXd xyCoeff = xCoeff*yCoeff.transpose();
			xyCoeff.resize(16,1);
			Vec current = ctrlPts*xyCoeff;
			
			Vector4d yDir = basisDir(dy*iy);
			MatrixXd xdy = xCoeff*yDir.transpose();
			xdy.resize(16,1);
			Vec tany = ctrlPts*xdy;
			
			MatrixXd ydx = xDir*yCoeff.transpose();
			ydx.resize(16,1);
			Vec tanx = ctrlPts*ydx;
			
			Vec normal = tany.cross(tanx).normalized();
			
			int currRowOff = (ix&1)<<1;
			int prevRowOff = (~ix&1)<<1;
			if (ix>0 && iy>0) {
				shared_ptr<Triangle> t1 = make_shared<Triangle>(
					prevs.col((iy-1<<2)+prevRowOff),
					prevs.col((iy<<2)+prevRowOff),
					current);
				t1->setNormals(
					prevs.col((iy-1<<2)+prevRowOff+1),
					prevs.col((iy<<2)+prevRowOff+1),
					normal);
				t1->matPtr = matPtr;
				faces.push_back(t1);
				shared_ptr<Triangle> t2 = make_shared<Triangle>(
					current,
					prevs.col((iy-1<<2)+currRowOff),
					prevs.col((iy-1<<2)+prevRowOff));
				t2->setNormals(
					normal,
					prevs.col((iy-1<<2)+currRowOff+1),
					prevs.col((iy-1<<2)+prevRowOff+1));
				faces.push_back(t2);
				t2->matPtr = matPtr;
			}
			prevs.col((iy<<2)+currRowOff) = current;
			prevs.col((iy<<2)+currRowOff+1) = normal;
		}
	}
}

#endif

