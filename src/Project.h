#ifndef PROJECT_H
#define PROJECT_H


#include <cmath>
#include <memory>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <limits>
#include <utility>
#include <initializer_list>
#include <random>
#include <chrono>
#include <iostream>
#include <fstream>

#include <Eigen/Geometry>


// Aliases
typedef Eigen::Vector3d Vec;
typedef Vec RGB;

// Usings

using std::sqrt;
using std::shared_ptr;
using std::make_shared;
using std::vector;
using std::partition;
using std::shuffle;
using std::pair;
using std::random_device;
using std::default_random_engine;
using std::uniform_real_distribution;
using std::string;

using std::ostream;
using std::cout;
using std::cerr;
using std::endl;
using std::ifstream;

using Eigen::Matrix;
using Eigen::Matrix3d;
using Eigen::Matrix4d;
using Eigen::MatrixXd;
using Eigen::Vector2d;
using Eigen::Vector4d;
using Eigen::Affine3d;
using Eigen::Scaling;
using Eigen::AngleAxisd;
using Eigen::Translation3d;
using Eigen::Quaterniond;

#endif
