#ifndef AABB_H
#define AABB_H

#include "Project.h"

class AABB {
public:
	Vec min = {HUGE_VAL,HUGE_VAL,HUGE_VAL};
	Vec max = {-HUGE_VAL,-HUGE_VAL,-HUGE_VAL};
	
	AABB() {}
	
	AABB(std::initializer_list<Vec> points) {
		for (auto p : points) {
			addPoint(p);
		}
	}
	
	Vec getCenter() const {
		return (min+max)/2;
	}
	
	/* 
	 * return false if not uninitialized
	 * or min,max is modified incorrectly.
	 */
	bool isValid() {
		return (min.array()<=max.array()).all();
	}
	
	void addPoint(const Vec& point) {
		min = min.cwiseMin(point);
		max = max.cwiseMax(point);
	}
	
	void join(const AABB& box) {
		addPoint(box.min);
		addPoint(box.max);
	}
	
	/*
	 * Same as hittable, but there is no HitRecord.
	 * Code from Shirley's book. If ray.dir is 0,
	 * it always return true.
	 */
	bool hit(const Ray& ray, double minT, double maxT) const {
		for (int i = 0; i < 3; ++i) {
			double invDir = 1/ray.dir[i];
			double t0 = (min[i] - ray.src[i]) * invDir;
			double t1 = (max[i] - ray.src[i]) * invDir;
	        if (invDir < 0) std::swap(t0, t1);
	        minT = t0 > minT ? t0 : minT;
	        maxT = t1 < maxT ? t1 : maxT;
	        if (maxT <= minT) return false;
    	}
		return true;
	}
};

#endif
