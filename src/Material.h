#ifndef MATERIAL_H
#define MATERIAL_H

#include "Hittable.h"

/**
 * Clamps the input color to [0,1] range.
 * @param clr - input color
 */
inline RGB clamp(const RGB& clr) {
	return clr.cwiseMax(0).cwiseMin(1);
}

class Material {
public:
	virtual RGB emitted() const {
		return {0,0,0};
	}
	
	/**
	 * Sample a scattered ray.
	 * @param in - incoming ray
	 * @param rec - hit record to read from
	 * @param lights - area light
	 * @param attenuation - weight per channel to attenuate
	 * @param scattered - a random sample of scattered ray
	 * returns whether a scattered ray is generated
	 */
	virtual bool scatter(
		const Ray& in, const HitRecord& rec, const Hittable& lights,
		RGB& attenuation, Ray& scattered) const {
		return false;
	}
};

class Emitter : public Material {
public:
	RGB clr;
	Emitter(const RGB& light) {
		clr = light;
	}
	virtual RGB emitted() const {
		return clr;
	}
};

class Mirror : public Material {
public:
	RGB clr;
	Mirror(const RGB& light) {
		clr = light;
	}
	bool scatter(
		const Ray& in, const HitRecord& rec, const Hittable& lights,
		RGB& attenuation, Ray& scattered) const {
		attenuation = clr;
		scattered = Ray(rec.pos,in.dir-2*rec.normal.dot(in.dir)*rec.normal);
		return true;
	}
};

//only support glass with no color for now...
class Glass : public Material {
public:
	double ir;
	Glass(double idxRef) {
		ir = idxRef;
	}
	bool scatter(
		const Ray& in, const HitRecord& rec, const Hittable& lights,
		RGB& attenuation, Ray& scattered) const {
		attenuation = {1,1,1};
		
		Vec nDir = in.dir.normalized();
		Vec nNormal = rec.normal;
		//dot < 0 if the ray shoots inside (on "front face")
		double cosTheta = rec.normal.dot(nDir);
		double idxRef;
		if (cosTheta<0) {
			idxRef = 1/ir;
			cosTheta = -cosTheta;
			nNormal = -nNormal;
		} else {
			idxRef = ir;
		}
		cosTheta = fmin(cosTheta,1);
		double sinTheta = sqrt(1 - cosTheta*cosTheta);
		Vec dir;
		if (idxRef * sinTheta > 1 ||
			Eigen::Array<double,1,1>::Random()[0] <
			reflectance(cosTheta, idxRef)*2-1) {
			dir = nDir-2*cosTheta*nNormal; 
		} else {
			Vec perp = (nDir-cosTheta*nNormal)*idxRef;
			Vec parallel = sqrt(fmax(0,1 - perp.squaredNorm())) * nNormal;
			dir = perp + parallel;
		}
		
		scattered = Ray(rec.pos, dir);
		return true;
	}
private:
	static double reflectance(double cosTheta, double idxRef) {
        // From Shirley's book
        double r0 = (1-idxRef) / (1+idxRef);
        r0 = r0*r0;
        return r0 + (1-r0)*pow((1 - cosTheta),5);
    }
};

class Lambertian : public Material {
public:
	const double W_LIGHT = 0.5;
	RGB clr;
	Lambertian(const RGB& attenuation) {
		clr = attenuation;
	}
	bool scatter(
		const Ray& in, const HitRecord& rec, const Hittable& lights,
		RGB& attenuation, Ray& scattered) const {
		
		if (Eigen::Array<double,1,1>::Random()[0] < W_LIGHT*2-1) {
			//Happens with probability W_LIGHT
			//Sample lights
			scattered = lights.sample(rec.pos);
		} else {
//			Random sample on surface of unit sphere
			Vec t;
			while (true) {
				t = Vec::Random();
				double r2 = t.squaredNorm();
				if (EPSILON < r2 && r2 < 1) break;
			}
			Vec dir = rec.normal+t.normalized();
			dir = dir.squaredNorm()<EPSILON? rec.normal : dir.normalized();
			scattered = {rec.pos, dir};
		}
		
		//Weight attenuation
		double c = rec.normal.dot(scattered.dir.normalized());
		double pdfScattered = (c <= 0) ? 0 : c/M_PI;
		double pdfMixed = lights.sample_pdf(scattered)*W_LIGHT +
			pdfScattered*(1-W_LIGHT);
		attenuation = clr * pdfScattered / pdfMixed;
		
		return true;
	}
};


/**
 * Same as Lambertian but 2 colors and a checker_size is given to generate checkers.
 */
class CheckeredLambertian : public Material {
    public:
	const double W_LIGHT = 0.5;
	RGB clr1, clr2;
    double chkr_size;
	CheckeredLambertian(const RGB& color1, const RGB& color2, const float&  checker_size) {
		clr1 = color1;
        clr2 = color2;
        chkr_size = checker_size;
	}
	bool scatter(
		const Ray& in, const HitRecord& rec, const Hittable& lights,
		RGB& attenuation, Ray& scattered) const {
		Lambertian delegate(checker_color(rec));
		return delegate.scatter(in, rec, lights, attenuation, scattered);
	}

    private:
    RGB checker_color(const HitRecord& rec) const {
        if (((int)(rec.pos.array()/chkr_size).floor().sum())%2) {
            return clr1;
        } else {
            return clr2;
        }
    }

};

const shared_ptr<Material> MAT_DEFAULT = make_shared<Lambertian>(Vec(0,1,0));

#endif